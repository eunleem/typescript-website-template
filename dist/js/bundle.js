/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	exports.__esModule = true;
	var testClass_1 = __webpack_require__(1);
	(function ($) {
	    console.log("jQuery loaded.");
	    var ins = new testClass_1.TestClass();
	    ins.hello();
	    $(".box").html("Hello");
	})(jQuery);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

	"use strict";
	exports.__esModule = true;
	var TestClass = (function () {
	    function TestClass() {
	        console.log("Class Constructed");
	    }
	    TestClass.prototype.hello = function () {
	        console.log("Hello World");
	    };
	    return TestClass;
	}());
	exports.TestClass = TestClass;


/***/ })
/******/ ]);